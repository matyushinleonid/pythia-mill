"""
  Pythia Mill
"""

from setuptools import setup, find_packages, Extension

from codecs import open
import os
import numpy as np


import ensurepythia
shared_location, include_location = ensurepythia.ensure_pythia()
print('Using %s and %s to build against Pythia.' % (shared_location, include_location))

def split_libs(var):
  return [
    path for path in var.split(':') if len(path) > 0
  ]

def get_includes():
  env = os.environ

  includes = []

  for k in ['CPATH', 'C_INCLUDE_PATH', 'INCLUDE_PATH']:
    if k in env:
      includes.extend(split_libs(env[k]))

  return includes + [include_location]

def get_library_dirs():
  env = os.environ

  libs = []

  for k in ['LD_LIBRARY_PATH']:
    if k in env:
      libs.extend(split_libs(env[k]))

  return libs + [shared_location]

from Cython.Build import cythonize

here = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(here, 'README.md'), encoding='utf-8') as f:
  long_description = f.read()

extra_compile_args=['-std=c++98', '-Ofast', '-D_GLIBCXX_USE_CXX11_ABI=0', '-g']
extra_link_args=['-g']


def discover_cython(root):
  import os

  def path_to_module(path: str, filename: str):
    rpath = os.path.relpath(path, root)
    module = filename.split('.')[0]
    return '%s.%s' % (rpath.replace(os.path.sep, '.'), module)

  for path, _, files in os.walk('src'):
    for file in files:
      if file.endswith('.pyx'):
        yield Extension(
          path_to_module(path, file), [os.path.join(path, file)],
          libraries=['stdc++', 'pythia8'],
          include_dirs=[np.get_include()] + get_includes(),
          library_dirs=get_library_dirs(),
          language='c++',
          extra_compile_args=extra_compile_args
        )


extensions = [
  Extension(
    'pythiamill.utils.pythiautils', ['src/pythiamill/utils/pythiautils.pyx'],
    libraries=['stdc++', 'pythia8'],
    include_dirs=[np.get_include()] + get_includes(),
    library_dirs=get_library_dirs(),
    language='c++',
    extra_compile_args=extra_compile_args,
    extra_link_args=extra_link_args
  ),

  Extension(
    'pythiamill.utils.detector', ['src/pythiamill/utils/detector.pyx'],
    libraries=['stdc++', 'pythia8'],
    include_dirs=[np.get_include()] + get_includes(),
    library_dirs=get_library_dirs(),
    language='c++',
    extra_compile_args=extra_compile_args,
    extra_link_args=extra_link_args
  ),

  Extension(
    'pythiamill.utils.sdetector', ['src/pythiamill/utils/sdetector.pyx'],
    libraries=['stdc++', 'pythia8'],
    include_dirs=[np.get_include()] + get_includes(),
    library_dirs=get_library_dirs(),
    language='c++',
    extra_compile_args=extra_compile_args,
    extra_link_args=extra_link_args
  ),

  Extension(
    'pythiamill.utils.stdetector', ['src/pythiamill/utils/stdetector.pyx'],
    libraries=['stdc++', 'pythia8'],
    include_dirs=[np.get_include()] + get_includes(),
    library_dirs=get_library_dirs(),
    language='c++',
    extra_compile_args=extra_compile_args,
    extra_link_args=extra_link_args
  ),

  Extension(
    'pythiamill.utils.spherical_tracker', ['src/pythiamill/utils/spherical_tracker.pyx'],
    libraries=['stdc++', 'pythia8'],
    include_dirs=[np.get_include()] + get_includes(),
    library_dirs=get_library_dirs(),
    language='c++',
    extra_compile_args=extra_compile_args,
    extra_link_args=extra_link_args
  ),

  Extension(
      'pythiamill.utils.elliptic_tracker', ['src/pythiamill/utils/elliptic_tracker.pyx'],
      libraries=['stdc++', 'pythia8'],
      include_dirs=[np.get_include()] + get_includes(),
      library_dirs=get_library_dirs(),
      language='c++',
      extra_compile_args=extra_compile_args,
      extra_link_args=extra_link_args
  ),

  Extension(
    'pythiamill.utils.pseudo_velo', ['src/pythiamill/utils/pseudo_velo.pyx'],
    libraries=['stdc++', 'pythia8'],
    include_dirs=[np.get_include()] + get_includes(),
    library_dirs=get_library_dirs(),
    language='c++',
    extra_compile_args=extra_compile_args,
    extra_link_args=extra_link_args
  ),

  Extension(
    'pythiamill.utils.tunemcdetector', [
      'src/pythiamill/utils/tunemcdetector.pyx',
      'src/pythiamill/utils/TuneMC.cpp',
    ],
    libraries=['stdc++', 'pythia8'],
    include_dirs=[np.get_include()] + get_includes(),
    library_dirs=get_library_dirs(),
    language='c++',
    extra_compile_args=extra_compile_args,
    extra_link_args=extra_link_args
  ),

  Extension(
    'pythiamill.utils.counter', [
      'src/pythiamill/utils/counter.pyx'
    ],
    libraries=['stdc++', 'pythia8'],
    include_dirs=[np.get_include()] + get_includes(),
    library_dirs=get_library_dirs(),
    language='c++',
    extra_compile_args=extra_compile_args,
    extra_link_args=extra_link_args
  ),

  Extension(
    'pythiamill.utils.pid', [
      'src/pythiamill/utils/pid.pyx'
    ],
    libraries=['stdc++', 'pythia8'],
    include_dirs=[np.get_include()] + get_includes(),
    library_dirs=get_library_dirs(),
    language='c++',
    extra_compile_args=extra_compile_args,
    extra_link_args=extra_link_args
  )
]

setup(
  name='pythiamill',

  version='1.0.1',

  description="""Pythia generator for python.""",

  long_description = long_description,

  url='https://gitlab.com/mborisyak/pythia-mill',

  author='Maxim Borisyak',
  author_email='maximus.been at gmail dot com',

  maintainer = 'Maxim Borisyak',
  maintainer_email = 'maximus.been at gmail dot com',

  license='MIT',

  classifiers=[
    'Development Status :: 4 - Beta',

    'Intended Audience :: Science/Research',

    'Topic :: Scientific/Engineering :: Physics',

    'License :: OSI Approved :: MIT License',

    'Programming Language :: Python :: 3',
  ],

  keywords='Pythia',

  packages=find_packages('src'),
  package_dir={'': 'src/'},

  extras_require={
    'dev': ['check-manifest'],
    'test': ['nose>=1.3.0'],
  },

  install_requires=[
    'numpy',
    'cython',
    'ensurepythia'
  ],

  ext_modules = cythonize(
    extensions,
    gdb_debug=True,
    compiler_directives={
      'embedsignature': True,
      'language_level' : 3
    }
  ),
)
