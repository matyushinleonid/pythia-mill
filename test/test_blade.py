import numpy as np
import multiprocessing as mp

N = 10

def test_blade():
  from pythiamill import pythia_blade
  from pythiamill.utils import SphericalTracker, PseudoVELO
  from pythiamill.utils.options import test_pythia_options

  ctx = mp.get_context('spawn')

  command_queue = ctx.Queue()
  result_queue = ctx.Queue()
  detector = PseudoVELO(5, 32, 1, 1)

  for _ in range(N):
    command_queue.put((1.0, ))
  command_queue.put(None)

  blade = pythia_blade(detector, command_queue, result_queue, options=test_pythia_options, batch_size=32)

  for i in range(N):
    _, arr = result_queue.get(block=True, timeout=5)
    print(i, np.sum(arr))

  print(result_queue.get(block=True))

def test_blade_process():
  from pythiamill import pythia_blade
  from pythiamill.utils import SphericalTracker
  from pythiamill.utils.options import test_pythia_options

  ctx = mp.get_context('spawn')

  command_queue = ctx.JoinableQueue()
  result_queue = ctx.JoinableQueue()
  detector = SphericalTracker(is_binary=False)

  p = ctx.Process(
    target=pythia_blade,
    kwargs=dict(
      detector_factory=detector,
      command_queue=command_queue,
      queue=result_queue,
      options=test_pythia_options,
      batch_size=32
    )
  )

  p.start()

  command_queue.put((1.0, ))
  command_queue.put(None)

  print('Is process alive:', p.is_alive())

  print(result_queue.get(block=True))
  print(result_queue.get(block=True))

  p.terminate()

