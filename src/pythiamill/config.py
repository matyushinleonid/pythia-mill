
__all__ = [
  'please_be_quiet',
  'monash'
]

please_be_quiet = [
  ### telling pythia to be quiet.
  'Print:quiet = on',
  'Init:showProcesses = off',
  'Init:showMultipartonInteractions = off',
  'Init:showChangedSettings = off',
  'Init:showChangedParticleData = off',
  'Next:numberCount=0',
  'Next:numberShowInfo=0',
  'Next:numberShowEvent=0',
  'Stat:showProcessLevel=off',
  'Stat:showErrors=off',
]

monash = [
  ### setting default parameters to Monash values
  ### all options are taken from https://arxiv.org/abs/1610.08328
  "Tune:ee = 7",
  "Beams:idA = 11",
  "Beams:idB = -11",
  "Beams:eCM = 91.2",
  "WeakSingleBoson:ffbar2gmZ = on",
  "23:onMode = off",
  "23:onIfMatch = 1 -1",
  "23:onIfMatch = 2 -2",
  "23:onIfMatch = 3 -3",
  "23:onIfMatch = 4 -4",
  "23:onIfMatch = 5 -5",
]

