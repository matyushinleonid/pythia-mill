from .pythiautils cimport Pythia, Particle, FLOAT, cppstring, string
from .detector cimport Detector

import cython
cimport cython

from libc.stdlib cimport malloc, free

ctypedef cnp.uint8_t uint8

cdef class PyPythia:
  cdef Pythia * pythia

  def __cinit__(self):
    self.pythia = new Pythia()

  cdef Pythia * get_pythia(self) nogil:
    return self.pythia

  def __dealloc__(self):
    del self.pythia

cdef char * _chars(bytes s):
  cdef char * str
  cdef int i

  str = <char *>malloc((len(s) + 1) * sizeof(char))

  for i in range(len(s)):
    str[i] = s[i]

  str[len(s)] = b'\0'
  return str

cpdef void configure_pythia(PyPythia pypythia, list options):
  cdef int opt_len = len(options)
  cdef cppstring cpp_str
  cdef int i, j
  cdef bytes py_str

  for i in range(opt_len):
    cpp_str = _chars(bytes(options[i], encoding='utf-8'))
    pypythia.get_pythia().readString(cpp_str)
  pypythia.get_pythia().init()


cpdef PyPythia launch_pythia(list options):
  cdef PyPythia pypythia = PyPythia()
  configure_pythia(pypythia, options)

  return pypythia


@cython.nonecheck(False)
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef void bind_detector(Detector detector, PyPythia pypythia):
  cdef Pythia * pythia = pypythia.get_pythia()
  detector.bind(pythia)

@cython.nonecheck(False)
@cython.boundscheck(False)
@cython.wraparound(False)
cpdef bool check_pythia(PyPythia pypythia):
  cdef Pythia * pythia = pypythia.get_pythia()
  return not pythia.next()

# @cython.boundscheck(False)
# @cython.wraparound(False)
cpdef void pythia_worker(Detector detector, PyPythia pypythia, FLOAT[:, :] buffer, tuple args):
  cdef Pythia * pythia = pypythia.get_pythia()
  detector.bind(pythia)

  cdef int i = 0
  while i < buffer.shape[0]:
    if not pythia.next():
      continue

    detector.view(buffer[i], args)
    i += 1