cimport cython
import cython
from .pythiautils cimport Pythia, Event, FLOAT
from .detector cimport Detector

cimport numpy as cnp
import numpy as np

from libc.math cimport sqrt, atanh, tanh, atan2, M_PI, floor
from libc.stdlib cimport rand, srand, RAND_MAX


cdef inline double abs(double x) nogil:
  return -x if x < 0 else x


cdef inline double intersection_scale(
        double o_, double ox, double oy, double oz,
        double p_, double px, double py, double pz,
        double Rx_sqr, double Ry_sqr, double Rz_sqr
) nogil:
  """
  Solves
    (ox + scale * px) ** 2 + (oy + scale * py) ** 2 + (oz + scale * pz) ** 2 = R ** 2
  for scale.
  """
  cdef double d
  cdef double scalar_prod_

  scalar_prod_ = px * ox / Rx_sqr + py * oy / Ry_sqr + pz * oz / Rz_sqr

  d = 4 * scalar_prod_ * scalar_prod_ -  4 * p_ * (o_ - 1)
  if d < 0.0:
    return -1.0
  else:
    return (1 / p_) * (0.5 * sqrt(d) - scalar_prod_)

ctypedef cnp.uint8_t uint8


class EllipticTrackerWrapper(object):
  """
  For pickle.
  """
  def __init__(self, is_binary=True, photon_detection=False, pseudorapidity_steps=32, phi_steps=32,
               max_pseudorapidity=5, Rx=1., Ry=1., Rz=1., energy_threshold=0.0):

    self.args = (
      (1 if is_binary else 0),
      (1 if photon_detection else 0),
      pseudorapidity_steps, phi_steps,
      max_pseudorapidity,
      Rx, Ry, Rz,
      energy_threshold,
    )

  def __call__(self):
    return EllipticTracker(*self.args)

  def event_size(self,):
    return self.args[2] * self.args[3]


cdef class EllipticTracker(Detector):
  def __init__(self, int is_binary, int photon_detection, int pseudorapidity_steps, int phi_steps,
               double max_pseudorapidity=5, double Rx=1., double Ry=1., double Rz=1., double energy_threshold=0.0):

    self.energy_threshold = energy_threshold
    self.is_binary = is_binary
    self.photon_detection = photon_detection

    self.pr_steps = pseudorapidity_steps
    self.phi_steps = phi_steps

    self.Rx, self.Ry, self.Rz = Rx, Ry, Rz

    self.max_pseudorapidity = max_pseudorapidity

  def event_size(self):
    return self.pr_steps * self.phi_steps

  @cython.boundscheck(False)
  @cython.overflowcheck(False)
  cpdef void view(self, FLOAT[:] buffer, tuple args):
    cdef double offset_x = 0.0
    cdef double offset_y = 0.0
    cdef double offset_z = 0.0

    if len(args) > 0:
      offsets = args[0]

    if len(offsets) > 0:
      offset_x = offsets[0]

    if len(offsets) > 1:
      offset_y = offsets[1]

    if len(offsets) > 2:
      offset_z = offsets[2]

    cdef Pythia * pythia = self.pythia

    ### ...
    cdef double max_pseudorapidity = self.max_pseudorapidity

    ### utility constant.
    cdef double max_tanh = tanh(max_pseudorapidity)

    ### number of steps in pseudorapidity axis
    cdef int pr_steps = self.pr_steps
    ### size of one pseudorapidity step
    cdef double pr_step = 2 * max_pseudorapidity / pr_steps

    ### the same for phi
    cdef int phi_steps = self.phi_steps
    cdef double phi_step = 2 * M_PI / phi_steps

    ### momentum
    cdef double px, py, pz

    ### origin coordinates
    cdef double ox, oy, oz

    ### decay (end) coordinates
    cdef double dx, dy, dz

    ### utility deltas
    cdef double ax, ay, az

    ### coordinates of intersection with the detector sphere
    cdef double ix, iy, iz

    ### pseudorapidity
    cdef double pr
    cdef double phi

    ### norm* of the origin vector, squared
    cdef double o_

    ### norm* of the decay vector, squared
    cdef double d_

    ### norm of the momentum vector, squared
    cdef double p

    ### norm, norm* of the delta vector, squared
    cdef double a, a_

    ### radius and squared radius of the VELO layer
    cdef double Rx =  self.Rx
    cdef double Ry =  self.Ry
    cdef double Rz =  self.Rz
    cdef double Rx_sqr =  Rx * Rx
    cdef double Ry_sqr =  Ry * Ry
    cdef double Rz_sqr =  Rz * Rz

    ### || o + scale * p || = R
    cdef double scale

    ### tanh of pseudorapidity
    ### pr = atanh(iz / R), thus th = iz / R
    cdef double th

    ### position of the cells in the grid
    cdef int pr_i, phi_i

    ### component of the momentum traverse to the pixel
    cdef double pt

    cdef int i
    cdef int indx

    buffer[:] = 0.0

    for i in range(pythia.event.size()):
      if pythia.event.at(i).e() < self.energy_threshold:
        continue

      px = pythia.event.at(i).px()
      py = pythia.event.at(i).py()
      pz = pythia.event.at(i).pz()

      p = px * px + py * py + pz * pz

      if p < 1.0e-12:
        ### I guess, nobody would miss such particles
        continue

      ox = pythia.event.at(i).xProd() + offset_x
      oy = pythia.event.at(i).yProd() + offset_y
      oz = pythia.event.at(i).zProd() + offset_z

      dx = pythia.event.at(i).xDec() + offset_x
      dy = pythia.event.at(i).yDec() + offset_y
      dz = pythia.event.at(i).zDec() + offset_z

      ax = dx - ox
      ay = dy - oy
      az = dz - oz

      a = ax * ax + ay * ay + az * az
      a_ = ax * ax / Rx_sqr + ay * ay / Ry_sqr + az * az / Rz_sqr

      if a < 1.0e-9:
        ### particle decayed immediately
        continue

      o_ = ox * ox / Rx_sqr + oy * oy / Ry_sqr + oz * oz / Rz_sqr
      d_ = dx * dx / Rx_sqr + dy * dy / Ry_sqr + dz * dz / Rz_sqr

      ### the particle originates and decays
      ### either outside or inside the detector
      if (o_ >= 1 and d_ >= 1) or (o_ <= 0 and d_ <= 0):
         continue

      ### solution of ||origin + scale * (decay - origin)|| = R for scale
      scale = intersection_scale(o_, ox, oy, oz, a_, ax, ay, az, Rx_sqr, Ry_sqr, Rz_sqr)

      if scale < 0.0 or scale > 1.0:
        ### this should not happen
        continue

      ### coordinates of intersection
      ix = ox + scale * ax
      iy = oy + scale * ay
      iz = oz + scale * az

      pt = px * ix + py * iy + pz * iz
      pt /= sqrt(ix * ix + iy * iy + iz * iz)

      ### ix ** 2 + iy ** 2 + iz ** 2 must sum to R ** 2
      th = abs(iz) / Rz

      ### to avoid expensive atanh call
      ### Note: tanh and atanh are monotonous.
      if th >= max_tanh:
        ### particle too close to the beam axis
        continue

      ### actual pseudorapidity (abs of it)
      pr = atanh(th)
      pr_i = <int> floor(pr / pr_step)

      ### the negative semi-sphere.
      if iz < 0:
        pr_i = -pr_i - 1

      pr_i += pr_steps // 2

      ### phi is just atan, pi shift is just to compensate for negative angels
      phi = atan2(iy, ix) + M_PI
      phi_i = <int> floor(phi / phi_step)

      ### tracker activation
      if pythia.event.at(i).isCharged():
        indx = pr_i * phi_steps + phi_i

        if self.is_binary:
          buffer[indx] = 1.0
        else:
          buffer[indx] += pt

      if pythia.event.at(i).id() == 22 and self.photon_detection > 0:
        if self.is_binary:
          buffer[indx] = 1.0
        else:
          buffer[indx] += p

        break
