cimport cython
import cython
from .pythiautils cimport Pythia, Event, FLOAT
from .detector cimport Detector

cimport numpy as cnp
import numpy as np

ctypedef cnp.uint8_t uint8

class CounterWrapper(object):
  """
  For pickle.
  """
  def __init__(self, particle_to_count):
    self.args = (particle_to_count, )

  def __call__(self):
    return Counter(*self.args)

  def event_size(self,):
    return len(self.particles_to_count) + 2

cdef class Counter(Detector):
  def __init__(self, list particle_to_count):
    self.particles_to_count = particle_to_count
    self.particle_index = dict([ (code, i + 2) for i, code in enumerate(self.particles_to_count) ])

  def event_size(self):
    return len(self.particles_to_count) + 2

  cpdef void view(self, FLOAT[:] buffer, tuple args):
    cdef Pythia * pythia = self.pythia

    cdef int code, indx, i, event_size

    buffer[:] = 0.0

    event_size = pythia.event.size()
    buffer[0] = event_size

    for i in range(event_size):
      code = pythia.event.at(i).id()
      if indx in self.particle_index:
        indx = self.particle_index[code]
      else:
        indx = 1

      buffer[indx] += 1.0
