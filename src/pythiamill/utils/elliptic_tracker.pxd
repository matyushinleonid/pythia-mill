cimport numpy as cnp

from .pythiautils cimport Pythia, float32, float64, FLOAT
from .detector cimport Detector

cdef class EllipticTracker(Detector):
  cdef int pr_steps
  cdef int phi_steps

  cdef int is_binary
  cdef int photon_detection

  cdef float64 Rx, Ry, Rz

  cdef float64 max_pseudorapidity
  cdef float64 energy_threshold

  cpdef void view(self, FLOAT[:] buffer, tuple args)
