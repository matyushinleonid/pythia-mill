from .utils import launch_pythia, pythia_worker

import numpy as np
import multiprocessing as mp
from multiprocessing import Process, current_process

import os
import signal

__all__ = [
  'PythiaMill',
  'CachedPythiaMill',
  'ParametrizedPythiaMill',
  'pythia_blade',

  'sample_parametrized'
]

def pythia_process(detector_factory, command_queue, queue, options, log=None, batch_size=1):
  import sys
  if current_process().name != 'MainProcess':
    pid = os.getpid()
    sys.stdout = open(os.path.join(log, 'log_%d.stdout' % pid), 'w') if log is not None else open(os.devnull, 'w')
    sys.stderr = open(os.path.join(log, 'log_%d.stderr' % pid), 'w') if log is not None else open(os.devnull, 'w')

  return pythia_blade(detector_factory, command_queue, queue, options, batch_size=batch_size)

def pythia_blade(detector_factory, command_queue, queue, options, batch_size=1):
  detector_instance = detector_factory()

  event_size = detector_instance.event_size()
  buffer = np.ndarray(shape=(batch_size, event_size), dtype='float32')
  pythia = launch_pythia(options)

  while True:
    args = command_queue.get(block=True)
    if args is None:
      queue.put(None, block=True)
      break

    try:
      pythia_worker(detector_instance, pythia, buffer, args)
      queue.put((args, buffer.copy()), block=False)
    except Exception as e:
      queue.put((args, e), block=False)

def PythiaBlade(detector_factory, command_queue, queue, options, batch_size=1):
  return Process(
    target=pythia_blade,
    args=(detector_factory, command_queue, queue, options, batch_size)
  )

class PythiaMillBase(object):
  def __init__(self, detector_factory, options, batch_size=16,
               n_workers=None, log=None, seed=None):

    if seed is not None:
      if any([ 'Random:seed' in option for option in options]):
        import warnings
        warnings.warn('randomize_seed is turned off as Pythia options contain seed settings.')
        seed = None
    else:
      if not any(['Random:seed' in option for option in options]):
        import warnings
        warnings.warn('`seed` argument is not specified and Pythia options does not contain `Random:seed` options. '
                      'This may result in duplicating samples.')

    if n_workers is None:
      cpu_count = os.cpu_count()
      n_workers = max(1, cpu_count - 1) if cpu_count is not None else 1

    if seed is not None:
      import random
      random.seed(seed)
      seeds = set()

      while len(seeds) < n_workers:
        seeds.add(random.randrange(1, 900000000))

      seeds = list(seeds)
      random.shuffle(seeds)
    else:
      seeds = list()

    ctx = mp.get_context('spawn')

    self.command_queue = ctx.Queue()
    self.queue = ctx.Queue()

    self.processes = [
      ctx.Process(
        target=pythia_process,
        kwargs=dict(
          detector_factory=detector_factory,
          command_queue=self.command_queue,
          queue=self.queue,
          log=log,
          options=options if seed is None else (options + ['Random:setSeed=on', 'Random:seed=%d' % seeds[i]]),
          batch_size=batch_size
        )
      )
      for i in range(n_workers)
    ]

    for p in self.processes:
      p.start()

  def terminate(self):
    if self.processes is None:
      return

    for p in self.processes:
      try:
        os.kill(p.pid, signal.SIGKILL)
      except Exception as e:
        print('Failed to stop pythia blade (pid %d), reason: %s' % (p.pid, e))

    self.processes = None

  def __del__(self):
    self.terminate()

  def shutdown(self):
    try:
      if self.processes is None:
        return

      for _ in self.processes:
        self.command_queue.put(None)

      stopped = 0
      while True:
        c = self.queue.get(block=True)

        if c is None:
          stopped += 1

        if stopped >= len(self.processes):
          break
    except Exception as e:
      import warnings
      warnings.warn('Failed to stop mill gracefully. Killing brutally.')
      warnings.warn('%s' % e)
    finally:
      self.terminate()
      self.processes = None


class CachedPythiaMill(PythiaMillBase):
  def __init__(self, detector_factory, options, detector_args=(), batch_size=16,
               cache_size=None, n_workers=None, log=None, seed=None):
    
    super(CachedPythiaMill, self).__init__(
      detector_factory=detector_factory,
      options=options,
      batch_size=batch_size,
      n_workers=n_workers,
      log=log,
      seed=seed
    )

    self.cache_size = (2 * len(self.processes)) if cache_size is None else cache_size
    self.detector_args = detector_args

    for _ in range(self.cache_size):
      self.command_queue.put(self.detector_args)

  def __iter__(self):
    return self

  def __next__(self):
    return self._sample()

  def next(self):
    return self._sample()

  def _sample(self):
    if self.processes is None:
      raise ValueError('Mill has already been stopped!')

    _, batch = self.queue.get(block=True)
    #self.queue.task_done()
    self.command_queue.put(self.detector_args)
    return batch

  def sample(self, size=None):
    if size is None:
      return self._sample()
    else:
      current_size = 0
      data = []
      while current_size < size:
        batch = self._sample()
        data.append(batch)
        current_size += batch.shape[0]

      return np.concatenate(data, axis=0)[:size]

  def __call__(self, size=None):
    return self.sample(size)


class ParametrizedPythiaMill(PythiaMillBase):
  def __init__(self, detector_factory, options, batch_size=16, n_workers=None, log=None, seed=None):
    super(ParametrizedPythiaMill, self).__init__(
      detector_factory=detector_factory,
      options=options,
      batch_size=batch_size,
      n_workers=n_workers,
      seed=seed,
      log=log
    )

    self._pending_requests = 0

  def pending_requests(self):
    return self._pending_requests


  def request(self, *args):
    if self.processes is None:
      raise ValueError('Mill has already been stopped!')

    self._pending_requests += 1
    self.command_queue.put(args)


  def retrieve(self):
    if self.processes is None:
      raise ValueError('Mill has already been stopped!')

    if self._pending_requests <= 0:
      raise ValueError('Attempt to retrieve without request! Consider calling `request` method first.')

    try:
      args, batch = self.queue.get(block=True)

      self._pending_requests -= 1

      return args, batch
    except:
      import warnings
      warnings.warn('An exception occurred while retrieving. Cleaning queue.')
      while not self.queue.empty():
        self.queue.get()

      raise


PythiaMill = CachedPythiaMill


def sample_parametrized(mill: ParametrizedPythiaMill, detector_configurations, progress=None):
  """
  Asynchronously samples from `mill` data given detector configurations.

  This function should be preferred to a request-retrieve for-loop as the latter
  does not take advantage of parallel execution.

  Note, that returned values does not preserve order of `detector_configurations`.

  :param mill: instance of `ParametrizedMill`;
  :param detector_configurations: a list of detector parameters;
  :param progress: if set to `tqdm` retrieval progress is shown.
  :return:
    - parameters: array of shape `<number of samples> x <parameters dim>`, parameters for each sample;
    - samples: array of shape `<number of samples> x 1 x 32 x 32`, sampled events.
  """
  if progress is None:
    progress = lambda x, *args, **kwargs: x

  try:
    ### sending requests to the queue
    for args in detector_configurations:
      mill.request(*args)

    ### retrieving results
    data = [
      mill.retrieve()
      for _ in progress(range(len(detector_configurations)))
    ]

    samples = np.vstack([samples for params, samples in data])
    params = np.vstack([np.array([params] * samples.shape[0], dtype='float32') for params, samples in data])

    return params, samples
  finally:
    while mill.pending_requests() > 0:
      mill.retrieve()

